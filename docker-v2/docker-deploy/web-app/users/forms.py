from django import forms
from django.forms import ModelForm
from django.contrib.auth.models import User
from .models import Driver
from django.contrib.auth.forms import UserCreationForm

vehicle_choice = (
    ("small(3 passengers)", "small(3 passengers)"),  # 3 people
    ("median(5 passengers)", "median(5 passengers)"),  # 5 people
)

class RegistrationForm(UserCreationForm):
    email=forms.EmailField(required=True)

    class Meta:
        model=User
        fields=['username','email','password1','password2']

class BasicUpdateInfoForm(ModelForm):
    email = forms.EmailField(required=True)

    class Meta:
        model=User
        fields=['username','email']

class DriverRegistrationForm(forms.Form):
    license_plate_number = forms.CharField(max_length=50)
    vehicle_type = forms.ChoiceField(choices=vehicle_choice, required=True)
    special_info = forms.CharField(max_length=200, required=False)

class DriverUpdateForm(forms.ModelForm):
    class Meta:
        model = Driver
        fields = ['license_plate_number', 'vehicle_type', 'special_info']



