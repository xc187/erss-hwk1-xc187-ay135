from .forms import RegistrationForm
from django.contrib import messages
from django.shortcuts import render,redirect
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.decorators import login_required
from .forms import RegistrationForm, DriverUpdateForm, BasicUpdateInfoForm,DriverRegistrationForm
from .models import Driver
def register(request):
    form = RegistrationForm()
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'You successfully created an account!')
            return redirect('login')
    return render(request, 'users/register.html', {'form':form})

def logIn(request):
    if request.method=='POST':
        name=request.POST.get('username')
        key=request.POST.get('password')

        user=authenticate(request,username=name,password=key)

        if user is not None:
            login(request,user)
            return redirect('home')
        else:
            messages.info(request, 'Login failure. Try again!')
            return render(request, 'users/login.html')

    return render(request,'users/login.html', {})

def logOut(request):
    logout(request)
    return redirect('login')


@login_required
def driver_register(request):
    return_driver = Driver.objects.filter(user=request.user).first()
    if return_driver:
        messages.success(request, f'You have already been a driver!')
        return redirect('driver_update')

    driver_form = DriverRegistrationForm()
    if request.method == 'POST':
        driver_form = DriverRegistrationForm(request.POST)
        if driver_form.is_valid():
            vehicle_type = driver_form.cleaned_data['vehicle_type']
            if vehicle_type == "small(3 passengers)":
                max_num = 3
            else:
                max_num = 5
            driver = Driver(
                user=request.user,
                license_plate_number = driver_form.cleaned_data['license_plate_number'],
                max_num=max_num,
                vehicle_type=vehicle_type,
                special_info=driver_form.cleaned_data['special_info'],
            )
            driver.save()
            messages.success(request, f'Successfully register as a driver!')
            return redirect('ride-home')
    return render(request, 'users/driver_register.html', {'driver_form': driver_form})

@login_required
def driver_update(request):

    return_driver = Driver.objects.filter(user=request.user).first()
    if not return_driver:
        messages.success(request, f'You need to register as driver first!')
        return redirect('driver_register')

    user_form = BasicUpdateInfoForm(instance=request.user)
    driver_form = DriverUpdateForm(instance=request.user.driver)
    if request.method == 'POST':
        user_form = BasicUpdateInfoForm(request.POST, instance=request.user)
        driver_form = DriverUpdateForm(request.POST, instance=request.user.driver)
        if user_form.is_valid() and driver_form.is_valid():
            user_form.save()
            driver_form.save()
            messages.success(request, f'Successfully update your driver account!')
            return redirect('driver_update')

    return render(request, 'users/driver_update.html', {'user_form': user_form,'driver_form': driver_form})




