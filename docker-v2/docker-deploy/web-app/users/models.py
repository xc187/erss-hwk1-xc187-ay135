from datetime import timezone, datetime

from django.db import models
from django.contrib.auth.models import User

vehicle_choice = (
    ("small", "small"), #3 people
    ("median", "median"), #5 people
)


class Driver(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    vehicle_type = models.CharField(max_length=20, choices=vehicle_choice, default='small')
    license_plate_number=models.CharField(max_length=50)
    max_num=models.IntegerField()
    special_info=models.CharField(max_length=200,blank=True)

    def __str__(self):
        return f'Driver {self.user.username}'


