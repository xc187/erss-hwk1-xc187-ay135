from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from datetime import datetime 
from django.urls import reverse
vehicle_choice = (
    ("small", "small"), #3 people
    ("median", "median"), #5 people
)

role_choice = (
    ("ride_owner", "ride_owner"),
    ("ride_sharer", "ride_sharer"),
)

class Ride(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    #user_role = models.CharField(max_length=20, choices=role_choice, default='ride_owner')
    destination_address = models.CharField(max_length=200)
    arrival_time = models.DateTimeField(default=timezone.now)
    passenger_number = models.PositiveIntegerField(default=1)
    status = models.CharField(max_length=200,default="open")
    shared = models.BooleanField()
    #optional
    vehicle_type = models.CharField(max_length=20, choices=vehicle_choice, default='small')
    special_request = models.CharField(max_length=200, blank=True)
    seats_left = models.PositiveIntegerField(default=0)
    share_name = models.CharField(default='', max_length=50, blank=True)
    share_num = models.PositiveIntegerField(default=0)
    driver_name = models.CharField(default='', max_length=50, blank=True)

    def __str__(self):
        return self.destination_address

    def get_absolute_url(self):
        return reverse('owner-list')

class Sharer(models.Model):
    sharer = models.ForeignKey(User, on_delete=models.CASCADE)
    destination_address = models.CharField(max_length=200)
    earliest_time = models.DateTimeField(default=datetime.now(),help_text='Format: 2021-01-11 12:00')
    latest_time= models.DateTimeField(default=datetime.now(),help_text='Format: 2021-01-11 14:00')
    passenger_number = models.PositiveIntegerField(default=1)

    def __str__(self):
        return self.destination_address

    def get_absolute_url(self):
        return reverse('sharer-list')
