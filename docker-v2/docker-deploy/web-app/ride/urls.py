from django.urls import path
from . import views
from .views import home,create_ride,OwnerUpdateView,OwnerListView, OwnerConListView,\
    sharer_search_ride,share_ride,SharerListView,SharerConListView,confirm_ride,complete_ride,\
    DriverCompleteListView,DriverViewListView,driver_page,DriverPage,DriverConListView
from users import views as users
from ride import views as ride


urlpatterns=[
path('createRide/', create_ride, name='rideowner-create'),
path('updateRide/<int:pk>/',OwnerUpdateView.as_view(),name='owner-update'),
path('ownerconlist/', OwnerConListView.as_view(),name='owner-con-list'),
path('ownerlist/',OwnerListView.as_view(),name='owner-list'),
path('sharerconlist/', SharerConListView.as_view(),name='sharer-con-list'),
path('sharerlist/', SharerListView.as_view(),name='sharer-list'),
path('home/', views.home,name='ride-home'),
path('search_ride/', sharer_search_ride, name='search_ride'),
path('driver_search_ride/', DriverConListView.as_view(), name='driver-search-ride'),
path('driver_page/', DriverPage, name='driver-page'),
path('driver_complete_ride/', DriverCompleteListView.as_view(), name='driver-complete-ride'),
path('driver_view_ride/', DriverViewListView.as_view(), name='driver-view-ride'),
path('join_ride/',share_ride,name='share-ride'),
path('confirm_ride/<int:rid>/',confirm_ride,name='confirm-ride'),
path('complete_ride/<int:rid>/',complete_ride,name='complete-ride'),
path('driver_register/', users.driver_register, name='driver_register'),
path('driver_page/',driver_page,name='driver_page')

]
'''
app_name = 'ride'


'''
