# Generated by Django 4.0.2 on 2022-02-07 23:59

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ride', '0009_alter_sharer_earliest_time_alter_sharer_latest_time'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sharer',
            name='earliest_time',
            field=models.DateTimeField(default=datetime.datetime(2022, 2, 7, 18, 59, 1, 139600), help_text='Format: 2021-01-11 12:00'),
        ),
        migrations.AlterField(
            model_name='sharer',
            name='latest_time',
            field=models.DateTimeField(default=datetime.datetime(2022, 2, 7, 18, 59, 1, 139624), help_text='Format: 2021-01-11 14:00'),
        ),
    ]
