# Generated by Django 4.0.1 on 2022-02-07 21:26

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ride', '0004_alter_sharer_earliest_time_alter_sharer_latest_time'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sharer',
            name='earliest_time',
            field=models.DateTimeField(default=datetime.datetime(2022, 2, 7, 16, 26, 53, 867883), help_text='Format: 2021-01-11 12:00'),
        ),
        migrations.AlterField(
            model_name='sharer',
            name='latest_time',
            field=models.DateTimeField(default=datetime.datetime(2022, 2, 7, 16, 26, 53, 867926), help_text='Format: 2021-01-11 14:00'),
        ),
    ]
