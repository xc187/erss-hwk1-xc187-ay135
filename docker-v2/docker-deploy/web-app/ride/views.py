from django.shortcuts import get_object_or_404, render
from .forms import RequestForm,SharerSearchForm
from .models import Ride,Sharer,User
from users.models import Driver
from django.http import HttpResponseRedirect
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.views.generic import (ListView, DetailView, CreateView, UpdateView, DeleteView)
from django.db.models import Q
from django.urls import reverse
from django.core.mail import send_mail
from django.conf import settings
from django.shortcuts import render,redirect
from django.contrib import messages


def home(request):
    return render(request, 'ride/home.html')

def driver_page(request):
    return render(request,'ride/driver_page.html')

#  owner 创建一个ride
def create_ride(request):
    form = RequestForm()
    if request.method=='POST':
        form = RequestForm(request.POST)
        if form.is_valid():
            passenger_number = form.cleaned_data['passenger_number']
            #vehicle_type = form.cleaned_data['vehicle_type']
            vehicle_type = form.cleaned_data['vehicle_type']
            shared = form.cleaned_data['shared']
            if shared:
                if vehicle_type == "small":
                    seats_left = 3 - passenger_number # 后续加错误检测
                else:
                    seats_left = 5 - passenger_number
            ride = Ride(
                owner=request.user,
                destination_address = form.cleaned_data['destination_address'],
                arrival_time = form.cleaned_data['arrival_time'],
                passenger_number = passenger_number,
                shared = shared,
                vehicle_type = vehicle_type,
                special_request = form.cleaned_data['special_request'],
                seats_left = seats_left,
            )
            ride.save()
            url = reverse('owner-list')
            return HttpResponseRedirect(url) #成功了跳的界面
    return render(request, 'ride/create_ride.html', {'form':form})

#  owner 更新一个ride
class OwnerUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model=Ride

    fields=['destination_address','arrival_time','passenger_number','shared','vehicle_type','special_request']

    def form_valid(self, form):
        template_name = 'ride/home.html'
        form.instance.owner=self.request.user
        return super().form_valid(form)

    def test_func(self):
        myride = self.get_object()
        if self.request.user == myride.owner:
            return True
        return True

# owner筛选 状态为Open/hold的订单 ：view open rides
class OwnerListView(ListView):
    template_name='ride/owner-list.html'
    def get_queryset(self):
        return Ride.objects.filter((Q(status='open')|Q(status='hold'))&Q(owner=self.request.user)).order_by('arrival_time')

#owner筛选 状态为confirmed的订单：view confirmed orders
class OwnerConListView(ListView):
    template_name='ride/confirmed-owner-list.html'
    def get_queryset(self):
        return Ride.objects.filter(Q(status='confirmed')&Q(owner=self.request.user)).order_by('arrival_time')

#sharer筛选 状态为open/hold的订单： view shared orders
class SharerListView(ListView):
    template_name='ride/sharer-list.html'
    def get_queryset(self):
        return Ride.objects.filter((Q(status='open')|Q(status='hold'))&Q(share_name=self.request.user)).order_by('arrival_time')

#sharer筛选 状态为confirmed的订单： view confirmed orders
class SharerConListView(ListView):
    template_name='ride/confirmed-sharer-list.html'
    def get_queryset(self):
        return Ride.objects.filter(Q(status='confirmed')&Q(share_name=self.request.user)).order_by('arrival_time')


def DriverPage(request):
    driver = Driver.objects.filter(user=request.user.id).first()
    if driver is None:
        messages.success(request, f'You need to register as driver first!')
        return redirect('driver_register')
    else:
        return render(request,'ride/driver_page.html')

class DriverConListView(ListView):
    template_name='ride/confirm-driver-list.html'
    def get_queryset(self):
        return Ride.objects.filter((Q(status='open')|Q(status='hold')),
                                 vehicle_type=self.request.user.driver.vehicle_type,
                                   special_request__in=['', self.request.user.driver.special_info]).exclude(owner=self.request.user).order_by('arrival_time')

#driver筛选 状态为confirmed的订单： view to be completed orders
class DriverCompleteListView(ListView):
    template_name='ride/complete-driver-list.html'
    def get_queryset(self):
        return Ride.objects.filter(Q(status='confirmed')&Q(driver_name=self.request.user.username)).order_by('arrival_time')

#dirver筛选 状态为complete的订单： view past orders
class DriverViewListView(ListView):
    template_name='ride/view-driver-list.html'
    def get_queryset(self):
        return Ride.objects.filter(Q(status='complete')&Q(driver_name=self.request.user.username)).order_by('arrival_time')

#driver改变状态：将订单状态改为complete
def complete_ride(request,rid):
    myride=Ride.objects.filter(pk=rid).first()
    myride.status='complete'
    myride.save()
    messages.success(request, f'You successfully confirmed a ride!')
    return render(request,'ride/driver_page.html')

#driver改变状态：将订单状态改为confirmed
def confirm_ride(request,rid):
    myride=Ride.objects.filter(pk=rid).first()
    myride.status='confirmed'
    myride.driver_name=request.user.username
    myride.save()
    list=[]
    list.append(myride.owner.email)
    if (User.objects.filter(username=myride.share_name).first()):
        list.append(User.objects.filter(username=myride.share_name).first().email)
    send_mail(
        'Hi,',
        'Thank you for using our system. This email is a reminder that your order has been confirmed',
        'qqqw123456wqqq@outlook.com',
        list,
        fail_silently=False
    )
    messages.success(request, f'You successfully confirmed a ride!')
    return render(request,'ride/driver_page.html')

#sharer查找ride
def sharer_search_ride(request):
    form = SharerSearchForm()
    if request.method == 'POST':
        form = SharerSearchForm(request.POST)
        if form.is_valid():
            destination_address = form.cleaned_data['destination_address']
            earliest_arrive_date = form.cleaned_data['earliest_arrive_date']
            latest_arrive_date = form.cleaned_data['latest_arrive_date']
            passenger_number = form.cleaned_data['passenger_number']

            search_rides = Ride.objects.filter(shared=True, status="open",
                                               destination_address=destination_address, seats_left__gte=passenger_number).exclude(owner=request.user)

            search_rides = search_rides.filter(arrival_time__gte=earliest_arrive_date, arrival_time__lte=latest_arrive_date)
            return render(request,'ride/search_ride.html',{'form': form,'search_rides': search_rides,'ride_exist': True,'size': passenger_number})

    return render(request, 'ride/search_ride.html', {'form': form, "ride_exist": False})

def share_ride(request):
    result=request.POST
    ride_id=result['ride_detail_id']
    ride_detail=Ride.objects.filter(id=ride_id).first()
    ride_detail.share_num=int(result['size'])
    ride_detail.seats_left-=int(result['size'])
    ride_detail.status='hold'
    ride_detail.share_name=request.user.username
    ride_detail.save()
    messages.success(request, f'You successfully joined a ride!')
    return render(request,'ride/home.html')
















