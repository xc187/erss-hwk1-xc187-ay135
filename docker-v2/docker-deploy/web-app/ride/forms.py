from django import forms
from django.forms.fields import DateTimeField

vehicle_choice = (
    ("small", "small"), #3 people
    ("median", "median"), #5 people
)

class DateTimeInput(forms.DateTimeInput):
    input_type = 'datetime-local'

class RequestForm(forms.Form):
    arrival_time = DateTimeField(
        input_formats=['%Y-%m-%dT%H:%M'],
        widget=DateTimeInput(
            format='%Y-%m-%dT%H:%M',
            attrs={'type': 'datetime-local'})
    )
    destination_address = forms.CharField(max_length=200, required=True)
    passenger_number = forms.IntegerField(min_value=1, max_value=5, required=True)
    shared = forms.BooleanField(required=True)
    vehicle_type = forms.ChoiceField(choices = vehicle_choice, required=False)
    special_request = forms.CharField(max_length=200, required=False)

'''
class DriverSearchForm(forms.Form):
    vehicle_type = forms.ChoiceField(choices = vehicle_choice, required=False)
    max_num = forms.IntegerField()
    special_info = forms.CharField(max_length=200, blank=True)
'''
class SharerSearchForm(forms.Form):
    destination_address = forms.CharField(max_length=200, required=True)
    earliest_arrive_date = DateTimeField(
        input_formats=['%Y-%m-%dT%H:%M'],
        widget=DateTimeInput(
            format='%Y-%m-%dT%H:%M',
            attrs={'type': 'datetime-local'})
    )
    latest_arrive_date = DateTimeField(
        input_formats=['%Y-%m-%dT%H:%M'],
        widget=DateTimeInput(
            format='%Y-%m-%dT%H:%M',
            attrs={'type': 'datetime-local'})
    )
    passenger_number = forms.IntegerField(min_value=1, max_value=4, required=True)



