"""mysitefile URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path,include
from django.contrib.auth.views import LoginView,LogoutView
from users import views as users
from ride import views as ride
from django.urls import path, include



urlpatterns = [
    #path('ride/',include('ride.urls')),
    #path('users/', include('users.urls')),
    #path('login/',LoginView.as_view(template_name='users/login.html'),name='login'),
    #path('logout/', LogoutView.as_view(), name='logout'),
    #path('logout/',auth_views.LogoutView.as_view(), name='logout'),
    path('login/',users.logIn,name='login'),
    path('logout/',users.logOut,name='logout'),
    path('register/',users.register, name='register'),
    path('admin/', admin.site.urls),
    path('', include('ride.urls')),
    path('home/', ride.home,name='home'),
    path('driver_register/', users.driver_register, name='driver_register'),
    path('driver_update/', users.driver_update, name='driver_update'),
    #path('createRide/',ride.create_ride),
]
